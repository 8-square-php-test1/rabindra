<?php
include('../include/connection.php');
include('../include/header.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <h2>User list</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>DVDNAME</th>
                    <th>CREATION_DATE</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                 $sql = "SELECT user.name,user.address,user.user_id,dvd.dvdName,dvd.rented_date
                 from user join dvd on user.dvdId=dvd.id group by rented_date";
                $query = $db->prepare($sql);
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_OBJ);
                $cnt=1;
                if($query->rowCount() > 0)
                 {
                foreach($results as $result)
                {   ?>
                <tr>
                    <td><?php echo htmlentities($cnt);?></td>
                    <td><?php echo htmlentities($result->name);?></td>
                    <td><?php echo htmlentities($result->address);?></td>
                    <td><?php echo htmlentities($result->dvdName);?></td>
                    <td><?php echo htmlentities($result->rented_date);?></td>
                    <td>
                        <a href="edituser.php?user_id=<?php echo htmlentities($result->user_id);?>">EDIT </a>
                        <a href="deleteuser.php?user_id=<?php echo htmlentities($result->user_id);?>">DELETE </a>
                    </td>
                </tr>
                <?php $cnt=$cnt+1;}} ?>

            </tbody>
        </table>
    </div>

</body>

</html>