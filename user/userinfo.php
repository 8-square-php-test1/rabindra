<?php
include('../include/connection.php');
include('../include/header.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>alluser</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <h2>User Information</h2>
        <div class="section3">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>DOB</th>
                        <th>Gender</th>
                        <th>Address</th>

                    </tr>
                </thead>
                <tbody>
                    <?php 
                 $sql = "SELECT * from user";
                $query = $db->prepare($sql);
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_OBJ);
                $cnt=1;
                if($query->rowCount() > 0)
                 {
                foreach($results as $result)
                {   ?>
                    <tr>
                        <td><?php echo htmlentities($cnt);?></td>
                        <td><?php echo htmlentities($result->name);?></td>
                        <td><?php echo htmlentities($result->email);?></td>
                        <td><?php echo htmlentities($result->DOB);?></td>
                        <td><?php echo htmlentities($result->gender);?></td>
                        <td><?php echo htmlentities($result->address);?></td>
                        <td> <a href="edituser.php?user_id=<?php echo htmlentities($result->user_id);?>">EDIT </a>
                            <a href="deleteuser.php?user_id=<?php echo htmlentities($result->user_id);?>">DELETE <a>
                        </td>
                    </tr>
                    <?php $cnt=$cnt+1;}} ?>

                </tbody>
            </table>
        </div>
    </div>

</body>

</html>