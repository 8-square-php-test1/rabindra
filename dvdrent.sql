-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2019 at 09:49 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dvdrent`
--

-- --------------------------------------------------------

--
-- Table structure for table `dvd`
--

CREATE TABLE `dvd` (
  `id` int(11) NOT NULL,
  `dvdName` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `rented_status` int(1) NOT NULL,
  `rented_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP
) ;

--
-- Dumping data for table `dvd`
--

INSERT INTO `dvd` (`id`, `dvdName`, `description`, `rented_status`, `rented_date`, `created_date`, `modified_date`) VALUES
(1, 'Captain America', 'Action movies with superheroes', 1, '2019-02-12 03:33:46.393010', '0000-00-00 00:00:00.000000', '2019-02-12 03:33:46.393010'),
(2, 'Hercules', 'Based on Power.', 1, '2019-02-12 04:35:17.184715', '0000-00-00 00:00:00.000000', '2019-02-12 04:35:17.184715'),
(3, 'Spider Man', 'Based on comic book', 1, '2019-02-12 08:46:14.649922', '0000-00-00 00:00:00.000000', '2019-02-12 08:46:14.649922');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL,
  `DOB` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` varchar(30) NOT NULL,
  `dvdId` varchar(30) NOT NULL,
  `subscribed` varchar(6) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifying_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `name`, `email`, `DOB`, `gender`, `address`, `dvdId`, `subscribed`, `creation_date`, `modifying_date`) VALUES
(7, 'Roshan Thapa', 'Rosh56@gmail.com', '2012-02-07', 'Male', 'Kumaripati.Kathmandu,Nepal', '1', 'YES', '2019-02-12 03:53:18', '0000-00-00 00:00:00'),
(8, 'Sonam Singh', 'Sona67@gmail.com', '2006-02-04', 'Male', 'Sanepa,Nepal', '2', 'YES', '2019-02-12 04:36:07', '0000-00-00 00:00:00'),
(9, 'anusharan adhikari', 'anusharan@gmail.com', '2012-02-14', 'Male', 'Sanga,Kavre,Nepal', '', 'YES', '2019-02-12 07:21:30', '0000-00-00 00:00:00'),
(10, 'Swostima Khadka', 'sls@gmail.com', '2019-02-08', 'Male', 'kathmandu', '', 'YES', '2019-02-12 07:36:53', '0000-00-00 00:00:00'),
(11, 'Harikrishna Sharma', 'sls@gmail.com', '2013-02-08', 'Male', 'Bhaktapur,Nepal', '3', 'YES', '2019-02-12 08:47:16', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `dvdId` (`dvdId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dvd`
--
ALTER TABLE `dvd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
