<?php
include('../include/header.php');
include('../include/connection.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>alluser</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <h2>DVDLIST</h2>
        <div class="section3">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Rented Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                 $sql = "SELECT * from dvd";
                $query = $db->prepare($sql);
                $query->execute();
                $results=$query->fetchAll(PDO::FETCH_OBJ);
                $cnt=1;
                if($query->rowCount() > 0)
                 {
                foreach($results as $result)
                {   ?>
                    <tr>
                        <td><?php echo htmlentities($cnt);?></td>
                        <td><?php echo htmlentities($result->dvdName);?></td>
                        <td><?php echo htmlentities($result->description);?></td>
                        <td><?php echo htmlentities($result->rented_status);?></td>
                        <td><?php echo htmlentities($result->rented_date);?></td>
                        <td>
                            <a href="editdvd.php?id=<?php echo htmlentities($result->id);?>">EDIT </a>
                            <a href="deletedvd.php?id=<?php echo htmlentities($result->id);?>">DELETE </a>
                        </td>
                    </tr>
                    <?php $cnt=$cnt+1;}} ?>

                </tbody>
            </table>
        </div>
    </div>

</body>

</html>