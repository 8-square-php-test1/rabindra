<?php
include('../include/header.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="section1">
            <h2>INSERT DVD INFORMATION</h2>
            <form method="post" action=" ">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="dvdName">Name:</label>

                            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="dvdName"
                                required="required">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="comment">Description:</label>
                            <textarea class="form-control" rows="10" id="description" placeholder="Enter description"
                                name="description" required="required"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="insert" name="insert" class="btn btn-success">INSERT</button>
                        </div>
                    </div>
            </form>
        </div>
        <div>
</body>

</html>