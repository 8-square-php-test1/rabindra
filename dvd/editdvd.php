<?php
include('../include/header.php');
include('../include/connection.php');
if (isset($_POST['insert']))
{
$id=$_POST['id'];
$dvdName= $_POST['dvdName'];
$description= $_POST['description'];


if($_POST['dvdName']!='')
{
try
{
$edit= $db->prepare("UPDATE dvd SET dvdName= :dvdName,description= :description WHERE id= :id");
$edit->bindParam(':id',$id);
$edit->bindParam(':dvdName',$dvdName);
$edit->bindParam(':description',$description);
$edit->execute();
echo"Inserted successfully";

}
catch (PDOException $e)
{
echo $e->getMessage();
}
}
else
{
$msg = "PLEASE FILL ALL FIELDS";
echo "<script type='text/javascript'>alert('$msg');</script>";
}

}
$id= 0;
$dvdName="";
if(isset($_GET['id'])){
$id=$_GET['id'];
$insert=$db->prepare('SELECT * FROM dvd where id= :id');
$insert->execute(array(':id'=>$id));
$row=$insert->fetch();
$id=$row['id'];
$dvdName=$row['dvdName'];
$description=$row['description'];
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="section1">
            <h2>INSERT DVD INFORMATION</h2>
            <form method="post">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?=$id;?>">
                            <label class="control-label" for="dvdName">Name:</label>

                            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="dvdName"
                                value="<?=$dvdName;?>" required="required">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="comment">Description:</label>
                            <textarea class="form-control" rows="10" id="description" placeholder="Enter description"
                                name="description" value="<?=$description;?>" required="required"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="submit" name="update" class="btn btn-success">UPDATE</button>
                        </div>
                    </div>
            </form>
        </div>
        <div>
</body>

</html>