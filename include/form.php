<!DOCTYPE html>
<html lang="en">

<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="section1">
            <h2>USER REGISTRATION</h2>
            <form method="post" action="">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="name">Name:</label>

                            <input type="name" class="form-control" id="name" placeholder="Enter Name" name="name"
                                required="required">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="name">Email:</label>
                            <input type="Email" class="form-control" id="Email" placeholder="Enter Email" name="email"
                                required="required">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="DOB">DOB:</label>

                            <input type="date" class="form-control" id="DOB" name="DOB" required="required">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="default" class="control-label" name=>Gender</label>
                            <input type="radio" name="gender" value="Male" required="required" checked="">Male
                            <input type="radio" name="gender" value="Female" required="required">Female <input
                                type="radio" name="gender" value="Other" required="required">Other
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="address">Address:</label>
                            <input type="address" class="form-control" id="address"
                                placeholder="Enter Address(place,country)" name="address" required="required">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="default" class="control-label">DVDRENT</label>

                            <select name="dvd" class="form-control" id="default">
                                <option value="">Select DVD</option>
                                <?php $sql = "SELECT * from dvd";
$query = $db->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
                                <option value="<?php echo htmlentities($result->id); ?>">
                                    <?php echo htmlentities($result->dvdName); ?>&nbsp;
                                </option>
                                <?php }} ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="default" class="control-label">Subscribed</label>
                            <input type="radio" name="subscribed" value="YES" required="required" checked="">YES
                            <input type="radio" name="subscribed" value="NO" required="required">NO
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-success">REGISTER</button>
                        </div>
                    </div>

            </form>
        </div>
    </div>
</body>

</html>