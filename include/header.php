<!DOCTYPE html>
<html lang="en">

<head>
    <?php
session_start();
?>
    <title>Dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
        integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
        integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
        integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous">
    </script>
</head>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">DVD Rental System</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About US</a></li>
                    <li><a href="Contact">Contact</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar"><br><br><br>
                    <li class="has-children"><a href="#">USER</a>
                        <ul class="child-nav">
                            <li class="child-nav"><a href="../user/adduser.php">Add User</a></li>
                            <li class="child-nav"><a href="../user/userinfo.php">User Information</a></li>

                            <li class="child-nav"><a href="../user/alluser.php">UserList</a></li>
                            <li class="child-nav"><a href="../user/userlist.php">UserDateList</li>
                        </ul>
                    </li>
                </ul>

                </ul>
                <ul class="nav nav-sidebar">
                    <li class="has-children"><a href="#">DVD</a>
                        <ul class="child-nav">
                            <li class="child-nav"><a href="../dvd/adddvd.php">Add DVD</a></li>
                            <li class="child-nav"><a href="../dvd/dvdlist.php">DVDLIST</a></li>
                        </ul>
                    </li>
                </ul><br><br>
                <ul class="nav nav-sidebar">
                    <li><a href="../dvd/dvdlist.php">SEARCH DVD</a></li>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>